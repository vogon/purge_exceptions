require 'nokogiri'
require 'open-uri'
require 'zip'

require_relative 'title'

module USCode
	DOWNLOAD_PAGE_URL = "http://uscode.house.gov/download/download.shtml"

	private
	def self.download_xml
		Nokogiri::HTML(open(DOWNLOAD_PAGE_URL))
	end

	private
	def self.archive_name(release_point)
		"xml_uscAll@#{release_point}.zip"
	end

	private
	def self.file_name_for_title(n)
		"xml/usc#{n}.xml"
	end

	private
	def self.title_for_file_name(name)
		m = /usc(.+)\.xml/.match(name)
		m[1] if m
	end

	private
	def self.ensure_downloaded!
		if !File::exists?(archive_name(latest_release_point))
			fail "TODO: actually download if we get here"
		end
	end

	private
	def self.archive_zip
		ensure_downloaded!

		if !@archive_zip
			@archive_zip = Zip::File.open(archive_name(latest_release_point))
		end

		@archive_zip
	end

	def self.latest_release_point
		dl = download_xml

		# extract release point from page
		release_point = dl.css('.releasepointinformation').text

		# extract PL number from release point
		/Public Law (\d+-\d+)/.match(release_point)[1]
	end

	def self.titles
		archive = archive_zip
		titles = []

		archive.each do |entry|
			title = title_for_file_name(entry.name)

			titles << title if title
		end

		titles
	end

	def self.title(n)
		archive = archive_zip

		entry = archive.get_entry(file_name_for_title(n))
		return Title::load_xml(entry.get_input_stream)
	end
end