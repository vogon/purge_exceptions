require 'nokogiri'

module USCode
	class Title
		def self.load_xml(io)
			xml = Nokogiri::XML(io)

			t = Title::new
			t.number = xml.css('meta docNumber').text
			t.heading = xml.css('title > heading').text

			return t
		end

		attr_accessor :number
		attr_accessor :heading
	end
end